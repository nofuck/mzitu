package crawlerpkg

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/PuerkitoBio/goquery"
)

func TestIndex(t *testing.T) {
	Index()
}

func TestGetCategory(t *testing.T) {
	categories := GetCategory()
	result, _ := json.Marshal(categories)
	fmt.Println(string(result))
}

func TestGetLatest(t *testing.T) {
	fmt.Println(GetLatest())
}

func TestGetNextPage(t *testing.T) {
	document, _ := goquery.NewDocument("http://www.mzitu.com/123063")
	fmt.Println(GetNextPage(document))
}

func TestGetMeizi(t *testing.T) {
	GetMeizi("http://www.mzitu.com/27448")
}

func TestDownloadImage(t *testing.T) {
	//DownloadImage("http://i.meizitu.net/2018/03/02d11.jpg")
}