package crawlerpkg

import (
	"io/ioutil"
	"log"
	"net/http"
	"github.com/PuerkitoBio/goquery"
	"strconv"
	"fmt"
)

// Category is index category
type Category struct {
	Title string `json:"title"`
	Href  string `json:"href"`
}

// Mzitu is image info
type Mzitu struct {
	URL         string `json:"url"`
	Title       string `json:"title"`
	ViewCount   string `json:"view_count"`
	ReleaseDate string `json:"release_date"`
	LazyImgURL  string `json:"lazy_img_url"`
}

// Index is get mzitu index
func Index() []byte {
	response, err := http.Get("http://www.mzitu.com")
	if err != nil {
		log.Fatal("get mzitu home page fail,", err)
	}
	if response.StatusCode != http.StatusOK {
		log.Fatal("get mzitu status code,", response.StatusCode)
	}
	body, err := ioutil.ReadAll(response.Body)
	defer response.Body.Close()
	if err != nil {
		log.Fatal("read body error,", err)
	}

	return body
}

// GetCategory is get mzitu home page category
func GetCategory() []Category {
	var categories []Category
	document, err := goquery.NewDocument("http://www.mzitu.com")
	if err != nil {
		log.Fatal("parse index html error,", err)
	}
	document.Find("#menu-nav li").Each(func(i int, s *goquery.Selection) {
		if i != 0 {
			title := s.Find("a").Text()
			href, _ := s.Find("a").Attr("href")
			category := Category{Title: title, Href: href}
			categories = append(categories, category)
		}
	})

	return categories
}

// GetLatest is index latest
func GetLatest() []Mzitu {
	url := "http://www.mzitu.com/"
	var mzitus []Mzitu
	document, err := goquery.NewDocument(url)
	if err != nil {
		log.Fatal("parse index html error,", err)
	}
	maxPage := GetPostMaxPage(document)

	for i := 1; i <= maxPage; i++ {
		index := url + strconv.Itoa(i)
		fmt.Println(index)
		document, _ := goquery.NewDocument(index)
		document.Find("#pins li").Each(func(i int, s *goquery.Selection) {
			lazyImgURL, _ := s.Find("a").Eq(0).Find("img").Attr("src")
			url, _ := s.Find("span").Eq(0).Find("a").Attr("href")
			title, _ := s.Find("span").Eq(0).Find("a").Attr("href")
			releaseDate := s.Find("span").Eq(1).Text()
			viewCount := s.Find("span").Eq(2).Text()

			mzitu := Mzitu{URL: url, Title: title, LazyImgURL: lazyImgURL, ReleaseDate: releaseDate, ViewCount: viewCount}
			Download(mzitu.URL)
			mzitus = append(mzitus, mzitu)
		})

	}

	return mzitus
}

// GetMaxPage is next page
func GetPostMaxPage(document *goquery.Document) int {
	page, _ := strconv.Atoi(document.Find("div.nav-links a").Eq(-2).Text())
	return page
}
