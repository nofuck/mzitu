package crawlerpkg

import (
	"errors"
	"log"
	"net/http"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"fmt"
	"strconv"
	"sync"
	"net/url"
	"os"
	"path/filepath"
	"io"
	"path"
)

var wg sync.WaitGroup

// Meizi is girl detail info
type Meizi struct {
	URL         string   `json:"url"`
	Tag         string   `json:"tag"`
	Title       string   `json:"title"`
	ReleaseTime string   `json:"release_time"`
	ViewCount   string   `json:"view_count"`
	ImgURL      []string `json:"img_url"`
	wg          sync.WaitGroup
}

// Tag is tag info
type Tag struct {
	Name string `json:"name"`
	URL  string `json:"url"`
}

// Page is page info
type Page struct {
	Total       int `json:"total"`
	CurrentPage int `json:"current_page"`
}

func NewMeizi(url string) *Meizi {
	meizi := &Meizi{URL: url}
	meizi.MeiZiInfo()
	return meizi
}

func (meizi *Meizi) MeiZiInfo() *Meizi {
	mz := GetMeizi(meizi.URL)
	meizi.ImgURL = mz.ImgURL
	meizi.ReleaseTime = mz.ReleaseTime
	meizi.Tag = mz.Tag
	meizi.ViewCount = mz.ViewCount
	meizi.Title = mz.Title
	return meizi
}

// GetMeizi is get meizi detail info, get all image by index url
func GetMeizi(index string) Meizi {
	var wg sync.WaitGroup
	document, err := goquery.NewDocument(index)
	if err != nil {
		log.Fatal("parse index html error,", err)
	}
	title := document.Find(".main-title").Text()
	tag := document.Find(".main-meta span").Eq(0).Find("a").Text()
	releaseTime := document.Find(".main-meta span").Eq(1).Text()
	viewCount := document.Find(".main-meta span").Eq(2).Text()
	var imageList []string
	maxPage := GetMaxPage(document)
	imageChan := make(chan string, maxPage)
	for i := 1; i <= maxPage; i++ {
		wg.Add(1)
		go GetImageURL(strings.Join([]string{index, strconv.Itoa(i)}, "/"), imageChan, &wg)
	}
	wg.Wait()
	close(imageChan)
	for img := range imageChan {
		imageList = append(imageList, img)
	}

	meizi := Meizi{Tag: tag, Title: title, ReleaseTime: releaseTime, ViewCount: viewCount, ImgURL: imageList}
	return meizi
}

// GetNextPage is next page
func GetNextPage(document *goquery.Document) (string, error) {
	s := document.Find("div.pagenavi a").Eq(-1)
	if strings.Compare("下一页»", s.Find("span").Text()) == 0 {
		url, _ := s.Attr("href")
		return url, nil
	}
	return "", errors.New("next page is null")
}

// GetMaxPage is next page
func GetMaxPage(document *goquery.Document) int {
	page, _ := strconv.Atoi(document.Find("div.pagenavi a").Eq(-2).Find("span").Text())
	return page
}

// GetImageURL is get image url from url
func GetImageURL(url string, images chan string, wg *sync.WaitGroup) {
	document, err := goquery.NewDocument(url)
	if err != nil {
		log.Fatal("parse image html error,", err)
	}
	s := document.Find(".main-image a img")
	imageURL, _ := s.Attr("src")
	images <- imageURL
	//DownloadImage(imageURL)
	wg.Done()
}

func (meizi *Meizi) DownloadImage() {
	//fmt.Println(meizi.ImgURL)
	for _, value := range meizi.ImgURL {
		//fmt.Println(value)
		wg.Add(1)
		go func(value, title string) {
			u, _ := url.Parse(value)
			separator := string(filepath.Separator)

			client := &http.Client{}
			request, _ := http.NewRequest(http.MethodGet, value, nil)
			request.Header.Set("Referer", "http://www.mzitu.com")
			response, _ := client.Do(request)
			prefix := strings.Replace(u.Path, "\\", "/", -1)
			filePath := os.Getenv("GOPATH") + separator + u.Host + prefix
			filePath = strings.Replace(filePath, "\\", "/", -1)
			dir := filepath.Dir(filePath) + separator + title
			dir = strings.Replace(dir, ",", "，", -1)
			dir = strings.Replace(dir, "?", "？", -1)

			_, err := os.Stat(dir)
			if err != nil && os.IsNotExist(err) {
				err := os.MkdirAll(dir, os.ModePerm)
				if err != nil {
					fmt.Printf("mkdir failed![%v]\n", err)
				} else {
					fmt.Printf("mkdir success!\n")
				}
			}

			f, err := os.Create(dir + separator + path.Base(filePath))
			defer f.Close()
			if err != nil {
				panic(err)
			}
			io.Copy(f, response.Body)
			wg.Done()
		}(value, meizi.Title)
	}
	wg.Wait()
	fmt.Println("图片已经下载完成")
}

func Download(url string) {
	meizi := NewMeizi(url)
	meizi.DownloadImage()
}
